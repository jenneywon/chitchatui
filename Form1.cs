﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : MetroForm
    {
        public Form RefToForm2 { get; set; }
        public Form RefToForm3 { get; set; }
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            //login button
            Form2 obj1 = new Form2();
            obj1.RefToForm1 = this;
            this.Visible = false;
            obj1.Show();

            this.Hide();
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            //sign up botton
            Form3 obj2 = new Form3();
            obj2.RefToForm1 = this;
            this.Visible = false;
            obj2.Show();

            this.Hide();
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            //Forgot password link
            //Make form5
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {
            //username input box
        }

        private void metroTextBox2_Click(object sender, EventArgs e)
        {
            //password input box
        }

        
    }
}
