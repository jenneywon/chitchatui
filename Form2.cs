﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : MetroForm
    {
        public Form RefToForm1 { get; set; }
        public Form RefToForm2 { get; set; }
        public Form RefToForm3 { get; set; }
        public Form RefToForm4 { get; set; }
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //Your profile picutre
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            //Logout button
            Form1 obj1 = new Form1();
            obj1.RefToForm2 = this;
            this.Visible = false;
            obj1.Show();

            this.Hide();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            //New Chat Create
            Form4 obj4 = new Form4();
            obj4.RefToForm2 = this;
            this.Visible = false;
            obj4.Show();

            this.Hide();
        }

        private void metroTile3_Click(object sender, EventArgs e)
        {
            //Join Chat
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            //Add Friend
        }

        private void metroTile4_Click(object sender, EventArgs e)
        {
            //Options
        }

        
    }
}
