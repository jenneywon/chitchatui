﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form3 : MetroForm
    {
        public Form RefToForm1 { get; set; }
        public Form RefToForm3 { get; set; }

        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            //Let's ChitChat button
            Form2 obj2 = new Form2();
            obj2.RefToForm3 = this;
            this.Visible = false;
            obj2.Show();

            this.Hide();
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            //Go Back button
            Form1 obj1 = new Form1();
            obj1.RefToForm3 = this;
            this.Visible = false;
            obj1.Show();

            this.Hide();
        }
    }
}
